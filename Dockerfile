FROM docker.io/python:3.7-alpine

RUN pip3 install --upgrade pip && pip3 install nullsmtpd
RUN mkdir -pv /var/log/nullsmtpd

EXPOSE 25
VOLUME /var/log/nullsmtpd


CMD ["nullsmtpd", "-H", "0.0.0.0", "-P", "25", "--no-fork",  "--mail-dir", "/var/log/nullsmtpd"]
