ORG ?= oz123
IMG ?= nullsmtpd
TAG ?= 0.1

.PHONY: build
build: Dockerfile
	docker build -t $(ORG)/$(IMG):$(TAG) .

.PHONY: run
run:
	docker run -p 2525:25 -v mail:/var/log/nullsmtpd $(ORG)/$(IMG):$(TAG)

